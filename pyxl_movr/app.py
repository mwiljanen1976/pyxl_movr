#!/usr/bin/python

import bottle
import os
import json
import uuid
import time

class PyxlMovrApp:
    def __init__(self):
        self.app = bottle.Bottle()
        self.redirect_url = ""
        self.base_uri = "/"

        self.work_path = ''

        self.init()
        self.app_router()

    def app_router(self):
        self.app.route( self.base_uri, callback=self.new_session )
        self.app.route( self.base_uri + "<session_id>", callback=self.main_index )
        self.app.route( self.base_uri + "img/<filename>", callback=self.static_img )
        self.app.route( self.base_uri + "click/<session_id>", callback=self.click_area )

    def new_session( self ):
        session_id = ''.join( str( uuid.uuid4() ).split('-') )
        if not os.path.exists( 'temp' ):
            os.mkdir( 'temp' )
            self.work_path = './temp/'

        session_data = { 'canvas_data' : {} }

        self.save( session_id, session_data )
        return bottle.redirect( "/{}".format( session_id ) )

    def click_area( self, session_id ):
        click_pos = [0,0]

        for query_item in dict( bottle.request.query ):
            if 'click_area.x' in query_item:
                click_pos[0] = int( bottle.request.query.get('click_area.x') )
            if 'click_area.y' in query_item:
                click_pos[1] = int( bottle.request.query.get('click_area.y') )

            print( query_item, click_pos )

        return bottle.template( "index", session_id=session_id, click_pos=click_pos )

    def save(self, session_id, session_data ):
        session_data['timestamp'] = time.time()
        with open( self.work_path + '{}.json'.format(session_id), 'w' ) as fp:
            fp.write( json.dumps( session_data ) )

    #def main_index( self, session_id ):
    #    return "Hello PyxlMovr App with session_id: {}".format( session_id )

    def main_index( self, session_id ):
        return bottle.template( "index", session_id=session_id, click_pos=[0,0] )

    def static_img( self, filename ):
        return bottle.static_file( filename, root="img/" )

    def init(self):
        pass

    def start(self):
        return self.app

pyxlmovr_app = PyxlMovrApp()
app = application = pyxlmovr_app.start()

if __name__ == "__main__":
    app.run( host="127.0.0.1", port=54321 )
