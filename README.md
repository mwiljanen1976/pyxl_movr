# PYXL_MOVR

This is the repo for my git repo for my DTube series at my channel:

https://d.tube/#!/c/wiljman76

To run the app:

0) Make a virtualenv with python3 like this:

   $ python3 -m venv venv

1) Install bottle.py with pip:

   $ venv/bin/pip install bottle

2) Run app with:

   $ venv/bin/python app.py

3) Open your browser to http://127.0.0.1:54321
